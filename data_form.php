<?php

// --- Dynamic variables
$action = "";
if (isset($_GET["action"]) && !empty($_GET["action"])) { $action = $_GET["action"]; }
$_GET["action"] = "";

$sn = "";
if (isset($_GET["sn"]) && !empty($_GET["sn"])) { $sn = $_GET["sn"]; }
$_GET["sn"] = "";
$givenname = "";
if (isset($_GET["givenname"]) && !empty($_GET["givenname"])) { $givenname = $_GET["givenname"]; }
$_GET["givenname"] = "";
$cn = "";
if (isset($_GET["cn"]) && !empty($_GET["cn"])) { $cn = $_GET["cn"]; }
$_GET["cn"] = "";
$descr = "";
if (isset($_GET["descr"]) && !empty($_GET["descr"])) { $descr = $_GET["descr"]; }
$_GET["descr"] = "";
$mail = "";
if (isset($_GET["mail"]) && !empty($_GET["mail"])) { $mail = $_GET["mail"]; }
$_GET["mail"] = "";
$uid = "";
if (isset($_GET["uid"]) && !empty($_GET["uid"])) { $uid = $_GET["uid"]; }
$_GET["uid"] = "";

$wich_attr = "";
if (isset($_GET["wich_attr"]) && !empty($_GET["wich_attr"])) { $wich_attr = $_GET["wich_attr"]; }
$_GET["wich_attr"] = "";

// --- Static variables
if (!isset($cpt_uid)) { // Reprise du principe du singleton
	$cpt_uid = 1500;
}
/*
	Each time we call this page (in every require),
	it will rise up so that there is no duplicate ;
	please do not restart the program
	without restart the whole LDAP entries.. :D
// */
$cpt_uid++;

// --- Fixed variables
$homeDirectory = "/home/".$uid;
$uidNumber = $cpt_uid;
$guidNumber = $cpt_uid;
$loginShell = "/bin/bash";

