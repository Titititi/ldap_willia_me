<?php
session_start();
include 'disconnect_bar.php'; // include globals

if (isset($_GET["ret"]) && $_GET["ret"] == "0") {
	echo
		'
			<div style="background-color:#B95565">Une erreur est survenue !</div>
		'
	;
}


//$filterExemple='(|(sn=$person*)(givenname=$person*))';
$filterExemple='(objectClass=posixGroup)';
$filter='objectClass=posixAccount';

$resSearch = ldap_search($link_identifier, $ldapBase, $filter);
// deprecated
ldap_sort($link_identifier, $resSearch, 'uid');


echo '<div>';
	echo '<table style="border: 3px ridge rgba(176, 166, 156);">';

		echo '<thead style="background-color:#4095CD;">';
			echo '<tr>';
			$fEntry = ldap_first_entry($link_identifier, $resSearch);
			$arrFAttrs = ldap_get_attributes($link_identifier, $fEntry);
			$i = 0;
			foreach ($arrFAttrs as $attr) {
				if ($i % 2) {
					echo '<td>';
					echo $attr;
					echo '</td>';
				}
				$i++;
			}
			echo '<td><button
				onclick="document.location.href=\''.$rootUrl.'entry_form.php?action=ajouter\'">
				Add</button></td>';
			echo '</tr>';
		echo '</thead>';

		echo '<tbody>';
			$currEntry = ldap_first_entry($link_identifier, $resSearch);
			while ($currEntry) {
				echo '<tr>';
				$currArrEntryAttrs = ldap_get_attributes($link_identifier, $currEntry);
				$currEntryAttr = ldap_first_attribute($link_identifier,
					$currEntry);
				$iAFA = 0;
				$i = 0;
				while ($i < $currArrEntryAttrs["count"]) {
					if (array_search($arrFAttrs[$iAFA],
							$currArrEntryAttrs) === FALSE) {
						echo '<td></td>';
						$iAFA++;
					} else {
						$currEntryAttrValue = ldap_get_values($link_identifier,
							$currEntry, $arrFAttrs[$iAFA]);
						echo '<td>'.$currEntryAttrValue[0].'</td>';
						$iAFA++;
						$i++;
					}
				}

				$tmp = ldap_get_values($link_identifier, $currEntry, "sn");
				$modifiable_sn = $tmp[0];
				$tmp = ldap_get_values($link_identifier, $currEntry, "givenName");
				$modifiable_givenName = $tmp[0];
				$tmp = ldap_get_values($link_identifier, $currEntry, "cn");
				$modifiable_cn = $tmp[0];
				$tmp = ldap_get_values($link_identifier, $currEntry, "description");
				$modifiable_description = $tmp[0];
				$tmp = ldap_get_values($link_identifier, $currEntry, "mail");
				$modifiable_mail = $tmp[0];
				$tmp = ldap_get_values($link_identifier, $currEntry, "uid");
				$modifiable_uid = $tmp[0];

				echo
					'
						<td>
							<button style="display:inline-block"
								onclick="document.location.href=\'entry_form.php?sn='.$modifiable_sn.'&givenname='.$modifiable_givenName.'&cn='.$modifiable_cn.'&descr='.$modifiable_description.'&mail='.$modifiable_mail.'&uid='.$modifiable_uid.'&action=modifier\'"
							>Modif!</button>
							<button style="display:inline-block"
								onclick="document.location.href=\'manageLDAP.php?action=delete&uid='.$modifiable_uid.'\'"
							>X</button>
						</td>
					</tr>
					'
				;
				$currEntry = ldap_next_entry($link_identifier,
				 	$currEntry);
			}

		echo '</tbody>';
	echo '</table>';
echo '</div>';

