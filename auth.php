<?php
session_start();

include './global_macros.php';

echo "Etablissement de la connxion en cours...<br />";

if ($link_identifier) {
 
 	// if ($_POST["login"] == 'admin')  {} // Usage ?

	if (myIsNotEmpty($_POST["login"]) && myIsNotEmpty($_POST["mdp"])) {
		$ldapUser = "cn=".htmlspecialchars($_POST["login"]).','.$ldapBase;
		$mdp = htmlspecialchars($_POST["mdp"]);
		$userExists = ldap_bind($link_identifier, $ldapUser, $mdp);

		if ($userExists) {
			echo "Authentification réussie, vous allez être redirigé vers l'application.";
			$_SESSION["login"] = htmlspecialchars($_POST["login"]);
			$_SESSION["mdp"] = $mdp;
			myRedirect("app.php");
		} else {
			echo "Echec de l'authentification, vous allez être redirigé vers la page d'accueil.";
			myRedirect("index.html");
		}
	}

} else {
	echo "Erreur lors de la connexion, vous allez être redirigé vers la page d'accueil.";
	myRedirect("index.html");
}

