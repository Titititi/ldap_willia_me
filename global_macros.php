<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$ldapHost = 'LDAP://localhost';
$ldapPort = 389;
$ldapAdCo = $ldapHost.":".$ldapPort;
$ldapBase = "dc=bla,dc=com";
$ldapUser = ""; // Will be valorised at the authentification
require_once './connect.php';
$link_identifier = $li;

$rootUrl = "http://localhost/api_ldap/";
function myRedirect($endStr) {
	sleep(1);
	header( "Location: ".$rootUrl.$endStr);
	//exit;
}

function myIsNotEmpty($var) {
	return !(isset($var) && empty($var));
}

