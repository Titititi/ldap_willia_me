<?php

include_once './global_macros.php';

$ldapDeco = ldap_unbind($link_identifier);

$decoPhrase = ($ldapDeco) ? 'Vous avez été déconnecté.' : 'La déconnexion a échoué';
echo $decoPhrase;

myRedirect("index.php");