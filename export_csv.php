<?php

include_once './global_macros.php';

function export_data_to_csv($data, $filename = 'export_csv', $delimiter = ';', $enclosure = '"') {

    header("Content-disposition: attachment; filename=$filename.csv");
    header("Content-Type: text/csv");

    $fp = fopen("php://output", 'w');
    fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    fputcsv($fp, array_keys($data[0]), $delimiter, $enclosure);

    foreach ($data as $fields) {
        fputcsv($fp, $fields, $delimiter, $enclosure);
    }

    fclose($fp);
}

myRedirect("index.php");