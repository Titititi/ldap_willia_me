<?php
session_start();

require 'global_macros.php';
require 'data_form.php';

$ret = "";


$li = ldap_connect($ldapAdCo)
	or die("Impossible de se connecter au serveur LDAP.");

ldap_set_option($li, LDAP_OPT_PROTOCOL_VERSION, 3);
$link_identifier = $li;

$osef = ldap_bind($link_identifier, "cn=".$_SESSION["login"].",dc=bla,dc=com", $_SESSION["mdp"]); // Pourquoi il s'était déco' !?!
// */

if ($action == "ajouter") {
	$entry["sn"] = $sn;
	$entry["givenName"] = $givenname;
	$entry["cn"] = $cn;
	$entry["description"] = $descr;
	$entry["mail"] = $mail;
	$entry["uid"] = $uid;
	/* il nous manque quelques params // */

	$dc = "uid=".$uid.",ou=people,dc=bla,dc=com";

	$ret = (ldap_add($link_identifier, $dc, $entry) === FALSE) ? "0" : "1";

}
if ($action == "modifier") {
	$entry["sn"] = $sn;
	$entry["givenName"] = $givenname;
	$entry["cn"] = $cn;
	$entry["description"] = $descr;
	$entry["mail"] = $mail;
	$entry["uid"] = $uid;

	$dc = "uid=".$uid.",ou=people,dc=bla,dc=com";

	$ret = (ldap_mod_replace($link_identifier, $dc, $entry) === FALSE) ? "0" : "1";

}
if ($action == "delete_attr") {
    $entry["sn"] = $sn;
    $entry["givenName"] = $givenname;
    $entry["cn"] = $cn;
    $entry["description"] = $descr;
    $entry["mail"] = $mail;
    $entry["uid"] = $uid;

    unset($entry[$wich_attr]);

    $dc = "uid=".$uid.",ou=people,dc=bla,dc=com";

    $ret = (ldap_mod_del($link_identifier, $dc, $entry) === FALSE) ? "0" : "1";

}
else if ($action == "delete") {

	$dc = "uid=".$uid.",ou=people,dc=bla,dc=com";

	$ret = (ldap_delete($link_identifier, $dc) === FALSE) ? "0" : "1";

}

echo
	'
		<script>document.location.href=\''.$rootUrl.'app.php?action='.$action.'&ret='.$ret.'\'</script>
	'
;
